<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        <?php
        $activemenu = (isset($data['active-menu'])) ? $data['active-menu'] : "";
        ?>
        E-Pharma <?php if($activemenu !="") echo "|| ".Str::ucfirst($activemenu); ?>
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->  
    <!--begin::Page Vendors -->
    <link href="{{url('/')}}/include/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors -->
    <link href="{{url('/')}}/include/default/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{url('/')}}/include/default/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{url('/')}}/include/default/assets/demo/default/media/img/logo/favicon.ico" />

    @yield('style')

</head>
<!-- end::Head -->
<!-- end::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
    
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">

        <!-- BEGIN: Header -->
        @include('admin.layouts.topbar')
        <!-- END: Header -->			

        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            <!-- BEGIN: Left Aside -->
            @include('admin.layouts.leftsidebar')
            <!-- END: Left Aside -->
            
			<div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                {{-- @include('admin.layouts.subheader') --}}
                <!-- END: Subheader -->
				
                @yield('content')       

            </div>
        </div>
        <!-- end:: Body -->

        <!-- begin::Footer -->
        @include('admin.layouts.footer')
        <!-- end::Footer -->'

    </div>
    <!-- end:: Page -->

    <!-- begin::Quick Sidebar -->
    @include('admin.layouts.quicksidebar')
    <!-- end::Quick Sidebar -->	

    <!--begin::Base Scripts -->
    <script src="{{url('/')}}/include/default/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="{{url('/')}}/include/default/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Base Scripts -->   
    <!--begin::Page Vendors -->
    <script src="{{url('/')}}/include/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
    <!--end::Page Vendors -->  
    <!--begin::Page Snippets -->
    <script src="{{url('/')}}/include/default/assets/app/js/dashboard.js" type="text/javascript"></script>
    <!--end::Page Snippets -->

    @yield('script')

</body>
<!-- end::Body -->

</html>